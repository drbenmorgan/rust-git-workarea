// Copyright 2016 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use crates::chrono::{DateTime, Utc};
use crates::regex::Regex;
use crates::tempdir::TempDir;

use error::*;
use git::{CommitId, GitContext, Identity};

use std::borrow::Cow;
use std::collections::hash_map::HashMap;
use std::ffi::OsStr;
use std::fmt::{self, Debug};
use std::fs::{self, File};
use std::io::{Read, Write};
use std::iter;
use std::marker::PhantomData;
use std::path::{Path, PathBuf};
use std::process::{Command, Stdio};

#[derive(Debug)]
/// Representation of merge conflict possibilities.
pub enum Conflict {
    /// A regular blob has conflicted.
    Path(PathBuf),
    /// A submodule points to a commit not merged into the target branch.
    SubmoduleNotMerged(PathBuf),
    /// The submodule points to a commit not present in the main repository.
    SubmoduleNotPresent(PathBuf),
    /// The submodule conflicts, but a resolution is available.
    ///
    /// This occurs when the submodule points to a commit not on the first-parent history of the
    /// target branch on both sides of the merge. The suggested commit is the oldest commit on the
    /// main branch which contains both branches.
    SubmoduleWithFix(PathBuf, CommitId),
}

impl Conflict {
    /// The path to the blob that for the conflict.
    pub fn path(&self) -> &Path {
        match *self {
            Conflict::Path(ref p) |
            Conflict::SubmoduleNotMerged(ref p) |
            Conflict::SubmoduleNotPresent(ref p) |
            Conflict::SubmoduleWithFix(ref p, _) => p,
        }
    }
}

impl PartialEq for Conflict {
    fn eq(&self, rhs: &Self) -> bool {
        self.path() == rhs.path()
    }
}

/// A command which has been prepared to create a merge commit.
pub struct MergeCommand<'a> {
    /// The merge command.
    command: Command,

    /// Phantom entry which is used to tie a merge command's lifetime to the `GitWorkArea` to which
    /// it applies.
    _phantom: PhantomData<&'a str>,
}

impl<'a> MergeCommand<'a> {
    /// Set the committer of the merge.
    pub fn committer(&mut self, committer: &Identity) -> &mut Self {
        self.command
            .env("GIT_COMMITTER_NAME", &committer.name)
            .env("GIT_COMMITTER_EMAIL", &committer.email);
        self
    }

    /// Set the authorship of the merge.
    pub fn author(&mut self, author: &Identity) -> &mut Self {
        self.command
            .env("GIT_AUTHOR_NAME", &author.name)
            .env("GIT_AUTHOR_EMAIL", &author.email);
        self
    }

    /// Set the authorship date of the merge.
    pub fn author_date(&mut self, when: DateTime<Utc>) -> &mut Self {
        self.command
            .env("GIT_AUTHOR_DATE", when.to_rfc2822());
        self
    }

    /// Commit the merge with the given commit message.
    ///
    /// Returns the ID of the merge commit itself.
    pub fn commit<M>(self, message: M) -> Result<CommitId>
        where M: AsRef<str>,
    {
        self.commit_impl(message.as_ref())
    }

    /// The implementation of the commit.
    ///
    /// This spawns the commit command, feeds the message in over its standard input, runs it and
    /// returns the new commit object's ID.
    fn commit_impl(mut self, message: &str) -> Result<CommitId> {
        let mut commit_tree = self.command
            .spawn()
            .chain_err(|| "failed to construct commit-tree command")?;

        {
            let commit_tree_stdin =
                commit_tree.stdin.as_mut().expect("expected commit-tree to have a stdin");
            commit_tree_stdin.write_all(message.as_bytes())
                .chain_err(|| {
                    ErrorKind::Git("failed to write the commit message to commit-tree".to_string())
                })?;
        }

        let commit_tree = commit_tree.wait_with_output()
            .chain_err(|| "failed to execute commit-tree command")?;
        if !commit_tree.status.success() {
            bail!(ErrorKind::Git(format!("failed to commit the merged tree: {}",
                                         String::from_utf8_lossy(&commit_tree.stderr))));
        }

        let merge_commit = String::from_utf8_lossy(&commit_tree.stdout);
        Ok(CommitId::new(merge_commit.trim()))
    }
}

impl<'a> Debug for MergeCommand<'a> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "MergeCommand")
    }
}

#[derive(Debug)]
/// The result of an attempted merge.
pub enum MergeResult<'a> {
    /// A merge conflict occurred.
    Conflict(Vec<Conflict>),
    /// The merge is ready to be committed.
    ///
    /// The command may be executed in order to create the commit from the merged tree.
    Ready(MergeCommand<'a>),
}

/// The configuration for submodules within the tree.
pub type SubmoduleConfig = HashMap<String, HashMap<String, String>>;

/// Intermediate type for setting up the workarea. Does not include submodules.
struct PreparingGitWorkArea {
    /// The context to work with.
    context: GitContext,
    /// The directory the workarea lives under.
    dir: TempDir,
}

#[derive(Debug)]
/// A representation of an empty workarea where actions which require a work tree and an index may
/// be preformed.
pub struct GitWorkArea {
    /// The context to work with.
    context: GitContext,
    /// The directory the workarea lives under.
    dir: TempDir,
    /// The submodule configuration for the workarea.
    submodule_config: SubmoduleConfig,
}

lazy_static! {
    // When reading `.gitmodules`, we need to extract configuration values. This regex matches it
    // and extracts the relevant parts.
    static ref SUBMODULE_CONFIG_RE: Regex =
        Regex::new(r"^submodule\.(?P<name>.*)\.(?P<key>[^=]*)=(?P<value>.*)$").unwrap();
}

/// A trait to abstract over both `GitWorkArea` and `PreparingGitWorkArea`.
trait WorkareaGitContext {
    /// The `git` command for the workarea.
    fn cmd(&self) -> Command;
}

/// Checkout a set of paths into a workarea.
fn checkout<I, P>(ctx: &WorkareaGitContext, paths: I) -> Result<()>
    where I: IntoIterator<Item = P>,
          P: AsRef<OsStr>,
{
    let ls_files = ctx.cmd()
        .arg("ls-files")
        .arg("--")
        .args(paths.into_iter())
        .output()
        .chain_err(|| "failed to construct ls-files command")?;
    if !ls_files.status.success() {
        bail!(ErrorKind::Git(format!("listing paths in the index: {}",
                                     String::from_utf8_lossy(&ls_files.stderr))));
    }

    checkout_files(ctx, &ls_files.stdout)
}

/// Checkout a set of files (in `git ls-files` format) into a workarea.
fn checkout_files(ctx: &WorkareaGitContext, files: &[u8]) -> Result<()> {
    let mut checkout_index = ctx.cmd()
        .arg("checkout-index")
        .arg("-f")
        .arg("-q")
        .arg("--stdin")
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .stderr(Stdio::piped())
        .spawn()
        .chain_err(|| "failed to construct checkout-index command")?;
    checkout_index.stdin
        .as_mut()
        .expect("expected checkout-index to have a stdin")
        .write_all(files)
        .chain_err(|| ErrorKind::Git("writing to checkout-index".to_string()))?;
    let res = checkout_index.wait().expect("expected checkout-index to execute successfully");
    if !res.success() {
        let mut stderr = String::new();
        checkout_index.stderr
            .as_mut()
            .expect("expected checkout-index to have a stderr")
            .read_to_string(&mut stderr)
            .chain_err(|| "failed to read from checkout-index")?;
        bail!(ErrorKind::Git(format!("running checkout-index: {}", stderr)));
    }

    // Update the index for the files we put into the context
    let mut update_index = ctx.cmd()
        .arg("update-index")
        .arg("--stdin")
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .stderr(Stdio::piped())
        .spawn()
        .chain_err(|| "failed to construct update-index command")?;
    update_index.stdin
        .as_mut()
        .expect("expected update-index to have a stdin")
        .write_all(files)
        .chain_err(|| ErrorKind::Git("writing to update-index".to_string()))?;
    let res = update_index.wait().expect("expected update-index to execute successfully");
    if !res.success() {
        let mut stderr = String::new();
        update_index.stderr
            .as_mut()
            .expect("expected update-index to have a stderr")
            .read_to_string(&mut stderr)
            .chain_err(|| "failed to read from update-index")?;
        bail!(ErrorKind::Git(format!("running update-index: {}", stderr)));
    }

    Ok(())
}

/// Get the file name of a path.
///
/// Used to simplify printing a filename.
fn file_name(path: &Path) -> Cow<str> {
    path.file_name().map_or_else(|| Cow::Borrowed("<unknown>"), OsStr::to_string_lossy)
}

impl PreparingGitWorkArea {
    /// Create an area for performing actions which require a work tree.
    fn new(context: GitContext, rev: &CommitId) -> Result<Self> {
        let tempdir = TempDir::new_in(context.gitdir(), "git-work-area")
            .chain_err(|| "failed to create temporary directory")?;

        let workarea = Self {
            context: context,
            dir: tempdir,
        };

        debug!(target: "git.workarea",
               "creating prepared workarea under {}",
               workarea.dir.path().display());

        fs::create_dir_all(workarea.work_tree())
            .chain_err(|| "failed to create the workarea directory")?;
        workarea.prepare(rev)?;

        debug!(target: "git.workarea",
               "created prepared workarea under {}",
               file_name(workarea.dir.path()));

        Ok(workarea)
    }

    /// Set up the index file such that it things everything is OK, but no files are actually on
    /// the filesystem. Also sets up `.gitmodules` since it needs to be on disk for further
    /// preparations.
    fn prepare(&self, rev: &CommitId) -> Result<()> {
        // Read the base into the temporary index
        let res = self.git()
            .arg("read-tree")
            .arg("-i")  // ignore the working tree
            .arg("-m")  // perform a merge
            .arg(rev.as_str())
            .output()
            .chain_err(|| "failed to construct read-tree command")?;
        if !res.status.success() {
            bail!(ErrorKind::Git(format!("reading the tree from {}: {}",
                                         rev,
                                         String::from_utf8_lossy(&res.stderr))));
        }

        // Make the index believe the working tree is fine.
        self.git()
            .arg("update-index")
            .arg("--refresh")
            .arg("--ignore-missing")
            .arg("--skip-worktree")
            .stdout(Stdio::null())
            .status()
            .chain_err(|| "failed to construct update-index command")?;
        // Explicitly do not check the return code; it is a failure.

        // Checkout .gitmodules so that submodules work.
        checkout(self, iter::once(".gitmodules"))
    }

    /// Run a git command in the workarea.
    fn git(&self) -> Command {
        let mut git = self.context.git();

        git.env("GIT_WORK_TREE", self.work_tree())
            .env("GIT_INDEX_FILE", self.index());

        git
    }

    /// Create a `SubmoduleConfig` for the repository.
    fn query_submodules(&self) -> Result<SubmoduleConfig> {
        let module_path = self.work_tree().join(".gitmodules");
        if !module_path.exists() {
            return Ok(SubmoduleConfig::new());
        }

        let config = self.git()
            .arg("config")
            .arg("-f")
            .arg(module_path)
            .arg("-l")
            .output()
            .chain_err(|| "failed to construct config command for submodules")?;
        if !config.status.success() {
            bail!(ErrorKind::Git(format!("reading the submodule configuration: {}",
                                         String::from_utf8_lossy(&config.stderr))));
        }
        let config = String::from_utf8_lossy(&config.stdout);

        let mut submodule_config = SubmoduleConfig::new();

        let captures = config.lines()
            .filter_map(|l| SUBMODULE_CONFIG_RE.captures(l));
        for capture in captures {
            submodule_config.entry(capture.name("name")
                    .expect("the submodule regex should have a 'name' group")
                    .as_str()
                    .to_string())
                .or_insert_with(HashMap::new)
                .insert(capture.name("key")
                            .expect("the submodule regex should have a 'key' group")
                            .as_str()
                            .to_string(),
                        capture.name("value")
                            .expect("the submodule regex should have a 'value' group")
                            .as_str()
                            .to_string());
        }

        let gitmoduledir = self.context.gitdir().join("modules");
        Ok(submodule_config.into_iter()
            .filter(|&(ref name, _)| gitmoduledir.join(name).exists())
            .collect())
    }

    /// The path to the index file for the work tree.
    fn index(&self) -> PathBuf {
        self.dir.path().join("index")
    }

    /// The path to the directory for the work tree.
    fn work_tree(&self) -> PathBuf {
        self.dir.path().join("work")
    }
}

impl WorkareaGitContext for PreparingGitWorkArea {
    fn cmd(&self) -> Command {
        self.git()
    }
}

impl GitWorkArea {
    /// Create an area for performing actions which require a work tree.
    pub fn new(context: GitContext, rev: &CommitId) -> Result<Self> {
        let intermediate = PreparingGitWorkArea::new(context, rev)?;

        let workarea = Self {
            submodule_config: intermediate.query_submodules()?,
            context: intermediate.context,
            dir: intermediate.dir,
        };

        debug!(target: "git.workarea",
               "creating prepared workarea with submodules under {}",
               workarea.dir.path().display());

        workarea.prepare_submodules()?;

        debug!(target: "git.workarea",
               "created prepared workarea with submodules under {}",
               file_name(workarea.dir.path()));

        Ok(workarea)
    }

    /// Prepare requested submodules for use.
    fn prepare_submodules(&self) -> Result<()> {
        if self.submodule_config.is_empty() {
            return Ok(());
        }

        debug!(target: "git.workarea",
               "preparing submodules for {}",
               file_name(self.dir.path()));

        for (name, config) in &self.submodule_config {
            let gitdir = self.context.gitdir().join("modules").join(name);

            if !gitdir.exists() {
                error!(target: "git.workarea",
                       "{}: submodule configuration for {} does not exist: {}",
                       file_name(self.dir.path()),
                       name,
                       gitdir.display());

                continue;
            }

            let path = config.get("path")
                .expect("the 'path` configuration for submodules is required.");
            let gitfiledir = self.work_tree().join(path);
            fs::create_dir_all(&gitfiledir).chain_err(|| {
                format!("failed to create the {} submodule directory for the workarea",
                        name)
            })?;

            let mut gitfile = File::create(gitfiledir.join(".git"))
                .chain_err(|| format!("failed to create the .git file for the {} module", name))?;
            write!(gitfile, "gitdir: {}\n", gitdir.display())
                .chain_err(|| format!("failed to write the .git file for the {} module", name))?;
        }

        Ok(())
    }

    /// Run a git command in the workarea.
    pub fn git(&self) -> Command {
        let mut git = self.context.git();

        git.env("GIT_WORK_TREE", self.work_tree())
            .env("GIT_INDEX_FILE", self.index());

        git
    }

    /// Figure out if there's a possible resolution for the submodule.
    fn submodule_conflict<P>(&self, path: P, ours: &CommitId, theirs: &CommitId) -> Result<Conflict>
        where P: AsRef<Path>,
    {
        let path = path.as_ref().to_path_buf();

        debug!(target: "git.workarea",
               "{} checking for a submodule conflict for {}",
               file_name(self.dir.path()),
               path.display());

        let branch_info = self.submodule_config
            .iter()
            .find(|&(_, config)| {
                config.get("path")
                    .map_or(false,
                            |submod_path| submod_path.as_str() == path.to_string_lossy())
            })
            .map(|(name, config)| {
                (name, config.get("branch").map_or("master", String::as_str))
            });

        let (name, branch) = if let Some((name, branch_name)) = branch_info {
            if branch_name == "." {
                // TODO: Pass the branch name we are working on down to here.
                debug!(target: "git.workarea",
                       "the `.` branch specifier for submodules is not supported for conflict \
                        resolution");

                return Ok(Conflict::Path(path));
            }

            (name, branch_name)
        } else {
            debug!(target: "git.workarea",
                   "no submodule configured for {}; cannot attempt smarter resolution",
                   path.display());

            return Ok(Conflict::Path(path));
        };

        let submodule_ctx = GitContext::new(self.gitdir().join("modules").join(name));

        // NOTE: The submodule is assumed to be kept up-to-date externally.
        let refs = submodule_ctx.git()
            .arg("rev-list")
            .arg("--first-parent")   // only look at first-parent history
            .arg("--reverse")        // start with oldest commits
            .arg(branch)
            .arg(format!("^{}", ours))
            .arg(format!("^{}", theirs))
            .output()
            .chain_err(|| {
                "failed to construct rev-list command for submodule conflict resolution"
            })?;
        if !refs.status.success() {
            return Ok(Conflict::SubmoduleNotPresent(path));
        }
        let refs = String::from_utf8_lossy(&refs.stdout);

        for hash in refs.lines() {
            let ours_ancestor = submodule_ctx.git()
                .arg("merge-base")
                .arg("--is-ancestor")
                .arg(ours.as_str())
                .arg(hash)
                .status()
                .chain_err(|| {
                    "failed to construct merge-base command for submodule conflict resolution"
                })?;
            let theirs_ancestor = submodule_ctx.git()
                .arg("merge-base")
                .arg("--is-ancestor")
                .arg(theirs.as_str())
                .arg(hash)
                .status()
                .chain_err(|| {
                    "failed to construct merge-base command for submodule conflict resolution"
                })?;

            if ours_ancestor.success() && theirs_ancestor.success() {
                return Ok(Conflict::SubmoduleWithFix(path, CommitId::new(hash)));
            }
        }

        Ok(Conflict::SubmoduleNotMerged(path))
    }

    /// Extract conflict information from the repository.
    fn conflict_information(&self) -> Result<Vec<Conflict>> {
        let ls_files = self.git()
            .arg("ls-files")
            .arg("-u")
            .output()
            .chain_err(|| "failed to construct ls-files command for conflict resolution")?;
        if !ls_files.status.success() {
            bail!(ErrorKind::Git(format!("listing unmerged files: {}",
                                         String::from_utf8_lossy(&ls_files.stderr))));
        }
        let conflicts = String::from_utf8_lossy(&ls_files.stdout);

        let mut conflict_info = Vec::new();

        // Submodule conflict info scratch space
        let mut ours = CommitId::new(String::new());

        for conflict in conflicts.lines() {
            let info = conflict.split_whitespace()
                .collect::<Vec<_>>();

            assert!(info.len() == 4,
                    "expected 4 entries for a conflict, received {}",
                    info.len());

            let permissions = info[0];
            let hash = info[1];
            let stage = info[2];
            let path = info[3];

            if permissions.starts_with("160000") {
                if stage == "1" {
                    // Nothing to do; we don't need to know the hash of the submodule at the
                    // mergebase of the two branches.
                    //old = hash.to_owned();
                } else if stage == "2" {
                    ours = CommitId::new(hash);
                } else if stage == "3" {
                    conflict_info.push(self.submodule_conflict(path, &ours, &CommitId::new(hash))?);
                }
            } else {
                conflict_info.push(Conflict::Path(Path::new(path).to_path_buf()));
            }
        }

        Ok(conflict_info)
    }

    /// Checkout paths from the index to the filesystem.
    ///
    /// Normally, files are not placed into the worktree, so checks which use other tools to
    /// inspect file contents do not work. This method checks out files to the working directory
    /// and fixes up Git's knowledge that they are there.
    ///
    /// All paths supported by Git's globbing and searching mechanisms are supported.
    pub fn checkout<I, P>(&mut self, paths: I) -> Result<()>
        where I: IntoIterator<Item = P>,
              P: AsRef<OsStr>,
    {
        checkout(self, paths)
    }

    /// Prepare a command to create a merge commit.
    ///
    /// The merge is performed, but only as a tree object. In order to create the actual commit
    /// object, a successful merge returns a command which should be executed to create the commit
    /// object. That commit object should then be stored in a reference using `git update-ref`.
    pub fn setup_merge<'a>(&'a self, bases: &[CommitId], base: &CommitId, topic: &CommitId)
                           -> Result<MergeResult<'a>> {
        let merge_recursive = self.git()
            .arg("merge-recursive")
            .args(&bases.iter()
                .map(CommitId::as_str)
                .collect::<Vec<_>>())
            .arg("--")
            .arg(base.as_str())
            .arg(topic.as_str())
            .output()
            .chain_err(|| "failed to construct merge command")?;
        if !merge_recursive.status.success() {
            return Ok(MergeResult::Conflict(self.conflict_information()?));
        }

        self.setup_merge_impl(base, topic)
    }

    /// Prepare a command to create a merge commit.
    ///
    /// The merge is performed, but only as a tree object. In order to create the actual commit
    /// object, a successful merge returns a command which should be executed to create the commit
    /// object. That commit object should then be stored in a reference using `git update-ref`.
    pub fn setup_update_merge<'a>(&'a self, base: &CommitId, topic: &CommitId)
                                  -> Result<MergeResult<'a>> {
        self.setup_merge_impl(base, topic)
    }

    /// Prepare a command to create a merge commit.
    ///
    /// This supports choosing the merge strategy.
    fn setup_merge_impl<'a>(&'a self, base: &CommitId, topic: &CommitId)
                            -> Result<MergeResult<'a>> {
        debug!(target: "git.workarea",
               "merging {} into {}",
               topic,
               base);

        let write_tree = self.git()
            .arg("write-tree")
            .output()
            .chain_err(|| "failed to construct write-tree command")?;
        if !write_tree.status.success() {
            bail!(ErrorKind::Git(format!("writing the tree object: {}",
                                         String::from_utf8_lossy(&write_tree.stderr))));
        }
        let merged_tree = String::from_utf8_lossy(&write_tree.stdout);
        let merged_tree = merged_tree.trim();

        let mut commit_tree = self.git();

        commit_tree.arg("commit-tree")
            .arg(merged_tree)
            .arg("-p")
            .arg(base.as_str())
            .arg("-p")
            .arg(topic.as_str())
            .stdin(Stdio::piped())
            .stdout(Stdio::piped());

        Ok(MergeResult::Ready(MergeCommand {
            command: commit_tree,
            _phantom: PhantomData,
        }))
    }

    /// The path to the index file for the workarea.
    fn index(&self) -> PathBuf {
        self.dir.path().join("index")
    }

    /// The path to the working directory for the workarea.
    fn work_tree(&self) -> PathBuf {
        self.dir.path().join("work")
    }

    /// Run a command from the work tree root.
    pub fn cd_to_work_tree<'a>(&self, cmd: &'a mut Command) -> &'a mut Command {
        cmd.current_dir(self.work_tree())
    }

    /// The path to the git repository.
    pub fn gitdir(&self) -> &Path {
        self.context.gitdir()
    }

    /// The submodule configuration for the repository.
    ///
    /// This is read from the `.gitmodules` file in the commit (if it exists).
    pub fn submodule_config(&self) -> &SubmoduleConfig {
        &self.submodule_config
    }

    #[cfg(test)]
    /// The path to the working directory for the workarea.
    ///
    /// Only exported for testing purposes.
    pub fn __work_tree(&self) -> PathBuf {
        self.work_tree()
    }
}

impl WorkareaGitContext for GitWorkArea {
    fn cmd(&self) -> Command {
        self.git()
    }
}
