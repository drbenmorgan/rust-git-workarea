// Copyright 2016 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

#![deny(missing_docs)]

//! Git workarea
//!
//! This crate implements a workarea given a bare git repository. A workarea is like a worktree
//! except that its on-disk representation is minimal so that operations are not constrained by
//! disk speed.

#[macro_use]
extern crate error_chain;

#[macro_use]
extern crate lazy_static;

#[macro_use]
extern crate log;

mod crates {
    // public
    pub extern crate chrono;
    // pub extern crate error_chain;

    // private
    pub extern crate log;
    pub extern crate regex;
    pub extern crate tempdir;

    #[cfg(test)]
    pub extern crate itertools;
}

mod error;
mod git;
mod prepare;

pub use error::Error;
pub use error::ErrorKind;
pub use error::ResultExt;
pub use error::Result;

pub use git::CommitId;
pub use git::GitContext;
pub use git::Identity;
pub use git::MergeStatus;

pub use prepare::Conflict;
pub use prepare::MergeCommand;
pub use prepare::MergeResult;
pub use prepare::GitWorkArea;
pub use prepare::SubmoduleConfig;

#[cfg(test)]
mod test;
