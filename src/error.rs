// Copyright 2016 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

error_chain! {
    errors {
        /// A git error occurred.
        Git(msg: String) {
            description("git error")
            display("git error: '{}'", msg)
        }

        /// An invalid ref was used.
        InvalidRef(msg: String) {
            description("invalid ref")
            display("invalid git ref: '{}'", msg)
        }
    }
}
